/**
 * Affiche la carte d'un pokémon passé en paramètre
 *
 * @param nameOrIndex nom ou index du pokémon à afficher
 */
function addPokemon(nameOrIndex) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `${ROOT_URL}/pokemon/${nameOrIndex}/`, true);
    xhr.addEventListener("load",
        function () {
            if (xhr.status === 200) {
                const data = JSON.parse(xhr.responseText);
                addPokemonCard(data);
            } else {
                console.log(Error(xhr.statusText));
            }
        }
    )
    xhr.send();
}


/**
 * Affiche les cartes correspondant à chacune des espèces apparaissant dans la chaîne d'évolution de l'espèce passée en paramètre
 *
 * @param nameOrIndex espèce de départ de la chaîne d'évolution
 */
function addEvolutionChain(nameOrIndex) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `${ROOT_URL}/pokemon-species/${nameOrIndex}/`, true);
    xhr.addEventListener("load", function () {
        if (xhr.status === 200) {
            const speciesData = JSON.parse(xhr.responseText);

            const xhr2 = new XMLHttpRequest();
            xhr2.open('GET', speciesData.evolution_chain.url, true);
            xhr2.addEventListener("load", function () {
                if (xhr2.status === 200) {
                    const evolutionData = JSON.parse(xhr2.responseText);
                    const speciesUrls = getSpeciesUrls(evolutionData.chain);

                    speciesUrls.forEach(url => {
                        const xhr3 = new XMLHttpRequest();
                        xhr3.open('GET', url, true);
                        xhr3.addEventListener("load", function () {
                            if (xhr3.status === 200) {
                                const speciesData = JSON.parse(xhr3.responseText);
                                const defaultVariety = speciesData.varieties.find(variety => variety.is_default) || speciesData.varieties[0];
                                addPokemon(defaultVariety.pokemon.name);
                            } else {
                                console.log(Error(xhr3.statusText));
                            }
                        });
                        xhr3.send();
                    });
                } else {
                    console.log(Error(xhr2.statusText));
                }
            });
            xhr2.send();
        } else {
            console.log(Error(xhr.statusText));
        }
    });
    xhr.send();
}

