/**
 * @param url adresse à laquelle envoyer la requête GET
 * @returns une promesse représentant let données JSON récupérées depuis l'URL passée en paramètre
 */
function getData(url) {
    return new Promise(function (resolve, reject) {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.addEventListener("load",
            function () {
                if (xhr.status === 200) {
                    resolve(JSON.parse(xhr.responseText));
                } else {
                    reject(Error(xhr.statusText));
                }
            }
        )
        xhr.send();
    });
}


/**
 * Affiche la carte d'un pokémon passé en paramètre
 * 
 * @param nameOrIndex nom ou index du pokémon à afficher
 */
function addPokemon(nameOrIndex) {
    getData(`${ROOT_URL}/pokemon/${nameOrIndex}/`)
        .then(data => addPokemonCard(data))
        .catch(error => console.log(error));
}


/**
 * Affiche les cartes correspondant à chacune des espèces apparaissant dans la chaîne d'évolution de l'espèce passée en paramètre
 * 
 * @param nameOrIndex espèce de départ de la chaîne d'évolution
 */
function addEvolutionChain(nameOrIndex) {
    getData(`${ROOT_URL}/pokemon-species/${nameOrIndex}/`)
        .then(speciesData => {
            getData(speciesData.evolution_chain.url)
                .then(evolutionData => {
                    const speciesUrls = getSpeciesUrls(evolutionData.chain);

                    speciesUrls.forEach(url => {
                        getData(url)
                            .then(speciesData => {
                                const defaultVariety = speciesData.varieties.find(variety => variety.is_default) || speciesData.varieties[0];
                                addPokemon(defaultVariety.pokemon.name);
                            })
                            .catch(error => console.log(error));
                    });
                })
                .catch(error => console.log(error));
        })
        .catch(error => console.log(error));
}
