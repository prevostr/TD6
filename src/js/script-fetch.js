/**
 * Affiche la carte d'un pokémon passé en paramètre
 * 
 * @param nameOrIndex nom ou index du pokémon à afficher
 */
function addPokemon(nameOrIndex) {
    fetch(`${ROOT_URL}/pokemon/${nameOrIndex}/`)
        .then(response => response.json())
        .then(data => addPokemonCard(data))
        .catch(error => console.log(error));
}


/**
 * Affiche les cartes correspondant à chacune des espèces apparaissant dans la chaîne d'évolution de l'espèce passée en paramètre
 * 
 * @param nameOrIndex espèce de départ de la chaîne d'évolution
 */
function addEvolutionChain(nameOrIndex) {
    fetch(`${ROOT_URL}/pokemon-species/${nameOrIndex}/`)
        .then(response => response.json())
        .then(speciesData => {
            fetch(speciesData.evolution_chain.url)
                .then(response => response.json())
                .then(evolutionData => {
                    const speciesUrls = getSpeciesUrls(evolutionData.chain);
                    speciesUrls.forEach(url => {
                        fetch(url)
                            .then(response => response.json())
                            .then(speciesData => {
                                const defaultVariety = speciesData.varieties.find(variety => variety.is_default) || speciesData.varieties[0];
                                addPokemon(defaultVariety.pokemon.name);
                            })
                            .catch(error => console.log(error));
                    });
                })
                .catch(error => console.log(error));
        })
        .catch(error => console.log(error));
}
