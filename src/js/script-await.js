/**
 * Affiche la carte d'un pokémon passé en paramètre
 * 
 * @param nameOrIndex nom ou index du pokémon à afficher
 */
async function addPokemon(nameOrIndex) {
    try {
        let req = await fetch(`${ROOT_URL}/pokemon/${nameOrIndex}/`);
        let data = await req.json();
        addPokemonCard(data);
    } catch (error) {
        console.log(error);
    }
}


/**
 * Affiche les cartes correspondant à chacune des espèces apparaissant dans la chaîne d'évolution de l'espèce passée en paramètre
 * 
 * @param nameOrIndex espèce de départ de la chaîne d'évolution
 */
async function addEvolutionChain(nameOrIndex) {
    try {
        let req = await fetch(`${ROOT_URL}/pokemon-species/${nameOrIndex}/`);
        let speciesData = await req.json();
        req = await fetch(speciesData.evolution_chain.url);
        let evolutionData = await req.json();
        const speciesUrls = getSpeciesUrls(evolutionData.chain);
        speciesUrls.forEach(async url => {
            let req = await fetch(url);
            let speciesData = await req.json();
            const defaultVariety = speciesData.varieties.find(variety => variety.is_default) || speciesData.varieties[0];
            addPokemon(defaultVariety.pokemon.name);
        });
    } catch (error) {
        console.log(error);
    }
}
